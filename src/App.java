import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        // 1 Làm tròn số
        double rate1 = 2.100212;
        double rate2 = 2100;

        System.out.println("Use Math.round()");
        // lam tron len gom 1 so thap phan, nhan va chia cho 10
        System.out.println((double) Math.round(rate1 * 10) / 10);
        // lam tron len gom 2 so thap phan, nhan va chia cho 100
        System.out.println((double) Math.round(rate1 * 100) / 100);
        // lam tron len gom 3 so thap phan, nhan va chia cho 1000
        System.out.println((double) Math.round(rate1 * 1000) / 1000);

        System.out.println("Su dung phuong thuc Math.ceil()");
        // lam tron len gom 1 so thap phan, nhan va chia cho 10
        System.out.println((double) Math.ceil(rate1 * 10) / 10);
        // lam tron len gom 2 so thap phan, nhan va chia cho 100
        System.out.println((double) Math.ceil(rate1 * 100) / 100);
        // lam tron len gom 3 so thap phan, nhan va chia cho 1000
        System.out.println((double) Math.ceil(rate1 * 1000) / 1000);

        System.out.println("Use Math.round()");
        // lam tron len gom 1 so thap phan, nhan va chia cho 10
        System.out.println((double) Math.round(rate2 * 10) / 10);
        // lam tron len gom 2 so thap phan, nhan va chia cho 100
        System.out.println((double) Math.round(rate2 * 100));
        // lam tron len gom 3 so thap phan, nhan va chia cho 1000
        System.out.println((double) Math.round(rate2 * 1000) / 1000);

        // 1 Làm tròn số
        double num1 = 2.130212;
        int n1 = 2;
        double roundedNum = Math.round(num1 * Math.pow(10, n1)) / Math.pow(10, n1);
        System.out.println("1: After rounded: " + roundedNum);

        // 2 Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
        int min = 1;
        int max = 10;
        // Generate random double value from 1 to 10
        System.out.println("2. Random value in double from " + min + " to " + max + ":");
        double random_double = Math.random() * (max - min + 1) + min;
        System.out.println(random_double);

        // Generate random int value from 1 to 10
        System.out.println("2. Random value in int from " + min + " to " + max + ":");
        int random_int = (int) (Math.random() * (max - min + 1) + min);
        System.out.println(random_int);

        // 3 Tính số pytago từ 2 số đã cho c2 = a2 + b2
        double x6 = 4.0;
        double y6 = 3.0;

        // compute Math.hypot()
        System.out.println("3. Java Pythagorean Theorem: " + Math.hypot(x6, y6)); // 5.0

        // 4 Kiểm tra số đã cho có phải số chính phương hay không c = a2
        Scanner scanner = new Scanner(System.in);
        double num = 64;
        scanner.close();
        if (checkPerfectSquare(num))
            System.out.println(num + " is square number");
        else
            System.out.println(num + " is not square number");

        // 5 Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
        for (int i = 32; i <= 37; i++) {
            if (i % 5 != 0) {
                continue;
            }

            System.out.println("5. Divide 5: " + i);
        }

        // 6 Kiểm tra đầu vào có phải số hay không
        String str6input1 = "abcd";
        try {
            Integer.parseInt(str6input1);
            System.out.println(str6input1 + " is a valid integer");
        } catch (NumberFormatException e) {
            System.out.println(str6input1 + " is not a valid integer");
        }

        String str6input2 = "12";
        try {
            Integer.parseInt(str6input2);
            System.out.println(str6input2 + " is a valid integer");
        } catch (NumberFormatException e) {
            System.out.println(str6input2 + " is not a valid integer");
        }

        // 7 Kiểm tra số đã cho có phải lũy thừa của 2 hay không
        // giống bài 4?
        int num7 = 16;
        if ((num7 & (num7 - 1)) == 0 && num7 > 0) {
            System.out.println(num7 + " is a power of 2.");
        } else {
            System.out.println(num7 + " is not a power of 2.");
        }

        // 8 Kiểm tra số đã cho có phải số tự nhiên hay không
        double num8 = 1.2;
        if (num8 == Math.floor(num8) && num8 >= 0) {
            System.out.println(num8 + " is a natural number.");
        } else {
            System.out.println(num8 + " is not a natural number.");
        }

        // 9 Thêm dấu , vào phần nghìn của mỗi số
        int num9 = 1000;
        String formattedNum = String.format("%,d", num9);
        System.out.println("9: After format: " + formattedNum);

        // 10 Chuyển số từ hệ thập phân về hệ nhị phân
        int num10 = 10;
        String binary = Integer.toBinaryString(num10);
        System.out.println("10. binary number: " + binary);

    }

    public static boolean checkPerfectSquare(double x) {
        double sq = Math.sqrt(x);
        return ((sq - Math.floor(sq)) == 0);
    }
}
